
                                            // function for CONST BUTTON

const da_const = () => {
    const eto_const = 1;
    console.log(typeof eto_const);
    alert("eto const");
}

                                            // function for LET BUTTON

const da_let = () => {
    let eto_let = 1;
    console.log(typeof eto_let);
    alert("eto let");
}

                                            // function for VAR BUTTON

const da_var = () => {
    var eto_var = 1;
    console.log(typeof eto_var);
    alert("eto var")
}

                                            // function for BOOLEAN BUTTON

const da_boolean = () => {
    let bol = true;
    console.log(typeof bol);
    alert(typeof bol);
}

                                            // function for UNDIFINED BUTTON

const da_undefined = () => {
    let und;
    console.log(typeof und);
    alert(typeof und);
}


                                            // function for OBJECT BUTTON

const da_object = () => {
    let obj = { name: "Вася" };
    console.log(typeof obj);
    alert(typeof obj);
}

                                            // function for NULL BUTTON

const da_null = () => {
    let nul = null;
    console.log("null");
    alert("null");
}

                                            // function for STRING BUTTON

const da_string = () => {
    let str = "JA STROKA";
    console.log(typeof str);
    alert(typeof str);
}
