const full_result = document.getElementById('full_result');
const result_num = document.getElementById('inputwindow');
const btn = document.getElementsByClassName('btn');
const btnclear = document.getElementsByClassName('btnclear');

function calculateResult() {
    const result = eval(document.getElementById('inputwindow').value);
    if (isFinite(result)) {
        result_num.value = result;
    }
    else {
        result_num.value = 'ПЕППА'
    }
}

function setValue(x) {
    result_num.value += x;
    full_result.value += x;
}

function clear() {
    result_num.value = '';
    full_result.value = '';
}

btnclear[0].onclick = () => clear();

for (let i = 0; i < btn.length; i++) { /* цикл внутри масива (где 'i' это индекс)и идет перебор класса 'btn' пока инндекс меньше чем length масива */
    const currentButton = btn[i].getAttribute('data-key');
    btn[i].onclick = () => setValue(currentButton);
}





/*
                                                  ( Та же самая функция что написана выше, только элегантным кодом, Минимализм! )
const inputArray = [
    document.getElementById('full_result'),
    document.getElementById('inputwindow')
];

const setValue = value => inputArray.forEach(item => value !== null ? item.value += value : item.value = '');

btnclear[0].onclick = () => setValue(null);
                                                  ( Та же самая функция что написана выше, только элегантным кодом, Минимализм! )



                                                  (пример обращения в индекс)
const someArray = [
  ['Max', 'Sanja', 'Den4ik', 'sasniplz'],
  [1, 2, 3],
  ['Vadja']
];

console.log(someArray[0][3]);
                                                  (пример обращения в индекс)
*/
